package edu.eci.cosw.spademo.models;

/**
 * Created by amoto on 2/5/17.
 */
public class Todo {
    private String description;
    private int priority;

    public Todo(){}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
