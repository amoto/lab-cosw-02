package edu.eci.cosw.spademo.services;

import edu.eci.cosw.spademo.models.Todo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by amoto on 2/5/17.
 */
@Service
public class InMemoryTodoServices implements TodoServices {

    public ArrayList<Todo> todos;

    public InMemoryTodoServices(){
        todos=new ArrayList<>();
    }

    @Override
    public ArrayList<Todo> getTodos() {
        return todos;
    }

    @Override
    public void addTodo(Todo todo) {
        todos.add(todo);
    }
}
