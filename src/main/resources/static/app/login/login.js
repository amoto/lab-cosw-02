'use strict';

angular.module('myApp.login', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'login/login.html',
    controller: 'LoginCtrl'
  });
}])

.controller('LoginCtrl', ['$scope', '$rootScope', '$http','$location', function($scope,$rootScope,$http,$location){
    var authenticate = function (credentials, callback) {

        var headers = credentials ? {authorization: "Basic "
                    + btoa(credentials.username + ":" + credentials.password)
        } : {};
        console.log(credentials);
        $http.get('/app/user', {headers: headers}).then(function (data) {
            console.log(data);
            if (data.data.name) {
                //window.alert('funcionó');
                $rootScope.authenticated = true;
            } else {
                //window.alert(' no funcionó');
                $rootScope.authenticated = false;
            }
            callback && callback();
        },function () {
        //window.alert("falló");
            $rootScope.authenticated = false;
            callback && callback();
        });

    };

    authenticate();
    $scope.credentials = {};
    $scope.login = function () {
        authenticate($scope.credentials, function () {
            if ($rootScope.authenticated) {
            window.alert("Autenticación exitosa");
                $location.path("/view1");
                $scope.error = false;
            } else {
            window.alert("Datos incorrectos");
                $location.path("/login");
                $scope.error = true;
            }
        });
    };

}]);