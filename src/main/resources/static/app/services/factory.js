'use strict';
angular.module('services.listFactory', ['ngRoute'])

    .factory('factory', function () {
        var data = {
            listado: []
        };
        return {
            getListado: function () {
                return data.listado;
            },
            addTodo: function (todo) {
                data.listado.push(todo);
            }};
    });
angular.module('services.resource', ['ngRoute', 'ngResource'])
    .factory('Items', function($resource) {
            return $resource('/todo');
        });