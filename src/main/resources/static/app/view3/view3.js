'use strict';

angular.module('myApp.view3', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view3', {
    templateUrl: 'view3/view3.html',
    controller: 'View3Ctrl'
  });
}])

.controller('View3Ctrl',  ['$scope', 'factory','Items', function($scope,factory,Items){
    //$scope.listado=factory.getListado();
    //console.log(Items.get());
    //console.log(Items.query());
    $scope.listado=Items.query();
    $scope.propertyName = 'description';
      $scope.reverse = true;
	
      $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
    $scope.propertyName = propertyName;
      };
}]);
