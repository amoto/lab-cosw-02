'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'services.listFactory',
  'services.resource',
  'myApp.view1',
  'myApp.view2',
  'myApp.view3',
  'myApp.login',
  'myApp.version'
]).
config(['$locationProvider', '$routeProvider', '$httpProvider', function($locationProvider, $routeProvider,$httpProvider) {
  $locationProvider.hashPrefix('!');
  $routeProvider.otherwise({redirectTo: '/login'});
  $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}])
.controller('LogoutCtrl', ['$scope', '$rootScope', '$http','$location', function($scope,$rootScope,$http,$location){
       $scope.logout = function () {
           $http.post('/logout', {}).then(function () {
               $rootScope.authenticated = false;
               $location.path("/");
           },function (data) {
               $rootScope.authenticated = false;
           });
       };
   }]);


